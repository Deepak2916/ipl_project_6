const fs = require('fs')
const csvToJson = require('csvtojson')
const path = require('path')
const { match } = require('assert')
// const { match } = require('assert')

const Outputfilepath = path.join(__dirname, '../public/output', '4-top-10-economical-bowlers-2015.json')
const deliveriesCsv = path.join(__dirname, '../data/deliveries.csv')
const matchesCsv = path.join(__dirname, '../data/matches.csv')

const top10EconomicalBowlers2015Middleware = (req, res, next) => {

    csvToJson()
        .fromFile(matchesCsv)
        .then(matchesData => {
            //         console
            let matchesIds2015 = matchesData
                .filter(match => {
                    return match.season === '2015'
                })
                .map(match => {
                    return match.id
                })
            // console.log(matchesIds2015.length);


            csvToJson()
                .fromFile(deliveriesCsv)
                .then(delivery => {

                    const deliveries2015 = delivery.filter((match) => {
                        return matchesIds2015.includes(match.match_id)
                    })
                        .reduce((result, delivery) => {
                            // console.log(delivery)
                            if (result[delivery.bowler]) {
                                result[delivery.bowler].runns += parseInt(delivery.total_runs)

                            } else {
                                result[delivery.bowler] = {}
                                result[delivery.bowler].runns = parseInt(delivery.total_runs)
                            }
                            if (delivery.wide_runs == '0' && delivery.noball_runs == '0') {
                                if (result[delivery.bowler].balls) {
                                    result[delivery.bowler].balls += 1
                                } else {
                                    result[delivery.bowler].balls = 1
                                }
                            }

                            return result
                        }, {})
                    let bowlerEconomy = Object.entries(deliveries2015).reduce((result, data) => {
                        let economy = (data[1].runns / data[1].balls) * 6
                        result.push([data[0], economy.toFixed(2)])
                        return result
                    }, [])
                        .sort((bowler1, bowler2) => {
                            return bowler1[1] - bowler2[1]
                        })
                        .filter((data, index) => {

                            return index < 10

                        })
                        .reduce((output, data) => {
                            output[data[0]] = data[1]
                            return output
                        }, {})

                    res.status(200)
                        .json(bowlerEconomy)
                        .end()


                })
        })
        .catch(err => {
            next({
                message: err,
                statusCode: 500
            })
        })
}
module.exports = top10EconomicalBowlers2015Middleware