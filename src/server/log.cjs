
const fs = require('fs')
const path = require('path')

const logMiddleware = (req, res, next) => {

    if (req.url === '/favicon.ico') {
        return res.status(204).json({ nope: true })
    }

    const date = new Date()
    const logfilePath = path.join(__dirname, 'logfile.log')
    const content = {
        id: req.id,
        url: req.url,
        method: req.method,
        date: date.toLocaleDateString('en-US', {
            timeZone: 'Asia/Kolkata',
            year: 'numeric',
            month: 'long',
            day: 'numeric'
        }),

        time: date.toLocaleString('en-US', {
            hour: 'numeric',
            minute: 'numeric',
            second: 'numeric',
            hour12: true,
            timeZone: 'Asia/Kolkata'
        })
    }

    fs.appendFile(logfilePath, JSON.stringify(content) + ',', err => {
        if (err) {
            next({
                message: err,
                statusCode: 500
            })
        }
    })
    next()


}

module.exports = logMiddleware