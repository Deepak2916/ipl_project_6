
const errorHandlingMiddleware = (err, req, res, next) => {

    const statusCode = err.statusCode || 500

    const responseBody = {
        error: {
            message: err.message || 'Internal Server Error',
        }
    }

    res.status(statusCode).json(responseBody).end()
}


module.exports = errorHandlingMiddleware
