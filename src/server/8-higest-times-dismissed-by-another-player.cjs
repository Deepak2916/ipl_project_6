const fs = require('fs')
const csvToJson = require('csvtojson')
const path = require('path')

const csvData = path.join(__dirname, '../data/deliveries.csv')
const Outputfilepath = path.join(__dirname, '../public/output', '8-higest-times-dismissed-by-another-player.json')


const highestTimesDismissedByAnotherPlayerMiddleware = (req, res, next) => {

    csvToJson()
        .fromFile(csvData)
        .then(matchesData => {

            let bowlersWithDismissal = matchesData.reduce((result, delivery) => {

                if (delivery.player_dismissed != '' && delivery.dusmissal_kind != 'run out') {
                    if (result[delivery.bowler]) {
                        if (result[delivery.bowler][delivery.player_dismissed]) {
                            result[delivery.bowler][delivery.player_dismissed] += 1
                        } else {
                            result[delivery.bowler][delivery.player_dismissed] = 1
                        }
                    }
                    else {
                        // result={}
                        result[delivery.bowler] = {}
                        result[delivery.bowler][delivery.player_dismissed] = 1
                    }
                }
                return result
            }, {})
            let arrayOfDismissals = Object.entries(bowlersWithDismissal).map(data => {

                let maxDismissal = Object.entries(data[1]).sort((player1, player2) => {
                    return player2[1] - player1[1]
                }).slice(0, 1)

                return [data[0], maxDismissal[0][0], maxDismissal[0][1]]
            })
            arrayOfDismissals = arrayOfDismissals.sort((array1, array2) => {
                // console.log(array1[2])
                return array2[2] - array1[2]
            })
                .slice(0, 1)
            let higestDismissal = {
                bowler: arrayOfDismissals[0][0],
                batsman: arrayOfDismissals[0][1],
                dismissalCount: arrayOfDismissals[0][2]
            }
            res.status(200)
                .json(higestDismissal)
                .end()
        })
        .catch(err => {
            next({
                message: err,
                statusCode: 500
            })
        })
}

module.exports = highestTimesDismissedByAnotherPlayerMiddleware