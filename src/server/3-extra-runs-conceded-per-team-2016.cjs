const fs = require('fs')
const path = require('path')
const csvToJson = require('csvtojson')

const deliveriesCsv = path.join(__dirname, '../data/deliveries.csv')
const matchesCsv = path.join(__dirname, '../data/matches.csv')

const extraRunsConcededPerTeam2016Middleware = (req, res, next) => {

    csvToJson()
        .fromFile(matchesCsv)
        .then(matchesData => {
            const matchesIds2016 = matchesData
                .filter(match => {
                    return match.season === '2016'
                })
                .map(match => {
                    return match.id
                })
            // console.log(matches2016)
            csvToJson()
                .fromFile(deliveriesCsv)
                .then(deliveriesData => {
                    let jsonData = deliveriesData.reduce((data, match) => {
                        if (matchesIds2016.includes(match.match_id)) {
                            // console.log(match)
                            if (data[match.bowling_team]) {
                                data[match.bowling_team] += parseInt(match.extra_runs)
                            } else {
                                data[match.bowling_team] = parseInt(match.extra_runs)
                            }
                        }
                        return data
                    }, {})

                    res.status(200)
                        .json(jsonData)
                        .end()
                })
        })
        .catch(err => {
            next({
                message: err,
                statusCode: 500
            })
        })
}

module.exports = extraRunsConcededPerTeam2016Middleware
