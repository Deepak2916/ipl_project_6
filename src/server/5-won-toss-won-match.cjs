const fs = require('fs')
const csvToJson = require('csvtojson')
const path = require('path')

const csvData = path.join(__dirname, '../data/matches.csv')
const Outputfilepath = path.join(__dirname, '../public/output', '5-won-toss-won-match.json')


const wontossWonMatchMiddleware = (req, res, next) => {

    csvToJson()
        .fromFile(csvData)
        .then(matchesData => {
            // console.log(matchesData[0])
            let output = matchesData.reduce((result, match) => {
                if (match.toss_winner === match.winner) {
                    if (result[match.toss_winner]) {
                        result[match.toss_winner] += 1
                    } else {
                        result[match.toss_winner] = 1
                    }
                }
                return result
            }, {})
            res.status(200)
                .json(output)
                .end()
        })
        .catch(err => {
            next({
                message: err,
                statusCode: 500
            })
        })
}

module.exports = wontossWonMatchMiddleware