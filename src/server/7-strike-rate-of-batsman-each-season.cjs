const fs = require('fs')
const csvToJson = require('csvtojson')
const path = require('path')
const { match } = require('assert')

const matchCsv = path.join(__dirname, '../data/matches.csv')
const deliveriesCsv = path.join(__dirname, '../data/deliveries.csv')
const Outputfilepath = path.join(__dirname, '../public/output', '7-strike-rate-of-batsman-each-season.json')

const strikeRateOfBatsmanEachSeasonMiddleware = (req, res, next) => {


    csvToJson()
        .fromFile(matchCsv)
        .then(matches => {
            let seasonsAndMatchIds = matches.reduce((result, match) => {

                result[match.id] = match.season

                return result
            }, {})


            csvToJson()
                .fromFile(deliveriesCsv)
                .then(deliveries => {

                    let batsmanStrikeRates = deliveries.map((delivery) => {

                        delivery.match_id = seasonsAndMatchIds[delivery.match_id]
                        return delivery
                    })
                        .reduce((result, delivery) => {
                            let ball = 1
                            let totalBalls = 0
                            let totalRuns = 0
                            if (delivery.wide_runs !== '0') {

                                ball = 0
                            }
                            if (result[delivery.batsman]) {
                                if (result[delivery.batsman][delivery.match_id]) {
                                    result[delivery.batsman][delivery.match_id]['runs'] += +delivery.batsman_runs
                                    result[delivery.batsman][delivery.match_id]['balls'] += ball
                                }
                                else {
                                    result[delivery.batsman][delivery.match_id] = {}
                                    result[delivery.batsman][delivery.match_id]['runs'] = +delivery.batsman_runs
                                    result[delivery.batsman][delivery.match_id]['balls'] = ball

                                }
                            } else {
                                result[delivery.batsman] = {}

                                result[delivery.batsman][delivery.match_id] = {}
                                result[delivery.batsman][delivery.match_id]['runs'] = +delivery.batsman_runs
                                result[delivery.batsman][delivery.match_id]['balls'] = ball

                            }
                            totalBalls = result[delivery.batsman][delivery.match_id]['balls']
                            totalRuns = result[delivery.batsman][delivery.match_id]['runs']

                            result[delivery.batsman][delivery.match_id].StrickeRate = ((totalRuns / totalBalls) * 100).toFixed(2)
                            return result
                        }, {})

                    res.status(200)
                        .json(batsmanStrikeRates)
                        .end()


                })


        })
        .catch(err => {
            next({
                message: err,
                statusCode: 500
            })
        })
}

module.exports = strikeRateOfBatsmanEachSeasonMiddleware