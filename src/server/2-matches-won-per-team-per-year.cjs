
const fs = require('fs')
const path = require('path')
const csvToJson = require('csvtojson')


const csvData = path.join(__dirname, '../data/matches.csv')

const matchesWonPerTeamPerYearMiddleware = (req, res, next) => {

    csvToJson()
        .fromFile(csvData)
        .then(matchesData => {
            let jsonData = matchesData.reduce((output, match) => {

                if (output[match.season]) {
                    if (output[match.season][match.winner]) {
                        output[match.season][match.winner] += 1
                    } else {
                        output[match.season][match.winner] = 1
                    }
                } else {
                    const obj = {}
                    obj[match.winner] = 1
                    output[match.season] = obj
                }
                return output
            }, {})

            res
                .status(200)
                .json(jsonData)
                .end()
        })
        .catch(err => {
            next({
                message: err,
                statusCode: 500
            })
        })
}

module.exports = matchesWonPerTeamPerYearMiddleware


