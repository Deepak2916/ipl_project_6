const fs = require('fs')
const path = require('path')

const showLogs = (req, res, next) => {
    const logFilePath = path.join(__dirname, 'logfile.log')
    fs.readFile(logFilePath, 'utf-8', (err, data) => {
        if (err) {
            next({
                message: err,
                statusCode: 500
            })
        } else {

            data = `[${data.slice(0, -1).trim()}]`

            const jsonData = JSON.parse(data)

            res.status(200).json(jsonData).end()
        }
    })


}

module.exports = showLogs