const express = require('express')
const path = require('path')
const app = express()

const router = express.Router()

const { PORT } = require('../../config.js')
const requestId = require('express-request-id')

app.use(requestId())

const errorHandlingMiddleware = require('./errorHandler.cjs')
const matchesPerYearMiddleware = require('./1-matches-per-year.cjs')
const matchesWonPerTeamPerYearMiddleware = require('./2-matches-won-per-team-per-year.cjs')
const extraRunsConcededPerTeam2016Middleware = require('./3-extra-runs-conceded-per-team-2016.cjs')
const top10EconomicalBowlers2015Middleware = require('./4-top-10-economical-bowlers-2015.cjs')
const wontossWonMatchMiddleware = require('./5-won-toss-won-match.cjs')
const highestPlayerOfMatchForSeasonMiddleware = require('./6-highest-player-of-match-for-season.cjs')
const strikeRateOfBatsmanEachSeasonMiddleware = require('./7-strike-rate-of-batsman-each-season.cjs')
const highestTimesDismissedByAnotherPlayerMiddleware = require('./8-higest-times-dismissed-by-another-player.cjs')
const bestEconomyInSuperOverMiddleware = require('./9-best-economy-in-super-over.cjs')
const logMiddleware = require('./log.cjs')
const showLogsMiddleware = require('./showLogs.cjs')

app.use(logMiddleware)

app.get('/', (req, res, next) => {
    res.sendFile(path.join(__dirname, '../public/views/homepage.html'))
})

router.get('/', (req, res, next) => {
    res.sendFile(path.join(__dirname, '../public/views/ipl.html'))
})


router.get('/matches-played-per-year', matchesPerYearMiddleware)
router.get('/matches-won-per-team-per-year', matchesWonPerTeamPerYearMiddleware)
router.get('/extra-runs-conceded-per-team-2016', extraRunsConcededPerTeam2016Middleware)
router.get('/top-10-economical-bowlers-2015',top10EconomicalBowlers2015Middleware)
router.get('/won-toss-won-match',wontossWonMatchMiddleware)
router.get('/highest-Player-of-match-for-season',highestPlayerOfMatchForSeasonMiddleware)
router.get('/strike-rate-of-batsman-each-season',strikeRateOfBatsmanEachSeasonMiddleware)
router.get('/highest-times-dismissed-by-another-player',highestTimesDismissedByAnotherPlayerMiddleware)
router.get('/best-economy-in-super-overs',bestEconomyInSuperOverMiddleware)
app.get('/log', showLogsMiddleware)


router.use(errorHandlingMiddleware)


app.use('/ipl', router)
app.use((req,res,next)=>{
    res.status(404).sendFile(path.join(__dirname, '../public/views/errorpage.html'))
})

app.listen(PORT, () => {
    console.log(`server running on ${PORT}`)
})