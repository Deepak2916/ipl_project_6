const fs = require('fs')
const csvToJson = require('csvtojson')
const path = require('path')
const { log } = require('console')

const deliveriesCsv = path.join(__dirname, '../data/deliveries.csv')
const Outputfilepath = path.join(__dirname, '../public/output', '9-best-economy-in-super-over.json')

const bestEconomyInSuperOverMiddleware = (req, res, next) => {

    csvToJson()
        .fromFile(deliveriesCsv)
        .then(deliveryData => {
            let output = deliveryData.reduce((result, data) => {

                if (data.is_super_over != 0) {
                    if (result[data.bowler]) {
                        result[data.bowler].runs += parseInt(data.total_runs)
                    } else {
                        result[data.bowler] = {}
                        result[data.bowler].runs = parseInt(data.total_runs)
                    }
                    if (data.wide_runs == '0' && data.noball_runs == '0') {
                        if (result[data.bowler].balls) {
                            result[data.bowler].balls += 1
                        } else {
                            result[data.bowler].balls = 1
                        }
                    }
                }
                return result
            }, {})
            let beatEconomyArray = Object.entries(output).map(data => {
                let economy = (data[1].runs / data[1].balls) * 6
                return [data[0], economy.toFixed(2)]
            })
                .sort((bowler1, bowler2) => {
                    return bowler1[1] - bowler2[1]
                })
                .splice(0, 1)
            let bestEconomy = {}
            bestEconomy[beatEconomyArray[0][0]] = beatEconomyArray[0][1]

            res.status(200)
                .json(bestEconomy)
                .end()
        })
        .catch(err => {
            next({
                message: err,
                statusCode: err
            })
        })

}

module.exports = bestEconomyInSuperOverMiddleware