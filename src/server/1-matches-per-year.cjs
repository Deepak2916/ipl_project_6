const fs = require('fs')
const csvToJson = require('csvtojson')
const path = require('path')

const csvData = path.join(__dirname, '../data/matches.csv')


const matchesPerYearMiddleware = (req, res, next) => {

    csvToJson()
        .fromFile(csvData)
        .then(matchesData => {
            let matchesPerYear = matchesData.reduce((result, match) => {
                if (result[match.season]) {
                    result[match.season] += 1
                } else {
                    result[match.season] = 1
                }
                return result
            }, {})

            res.status(200)
                .json(matchesPerYear)
                .end()


        }).catch(err => {
            next({
                message: err,
                statuCode: 500
            })
        })

}

module.exports = matchesPerYearMiddleware