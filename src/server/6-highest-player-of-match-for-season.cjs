const fs = require('fs')
const csvToJson = require('csvtojson')
const path = require('path')

const csvData = path.join(__dirname, '../data/matches.csv')
const Outputfilepath = path.join(__dirname, '../public/output', '6-highest-player-of-match-for-season.json')


const highestPlayerOfMatchForSeasonMiddleware = (req, res, next) => {


    csvToJson()
        .fromFile(csvData)
        .then(matchesData => {
            // console.log(matchesData)
            const player_of_matchs = matchesData.reduce((result, match) => {
                if (result[match.season]) {
                    if (result[match.season][match.player_of_match]) {
                        result[match.season][match.player_of_match] += 1
                    }
                    else {
                        result[match.season][match.player_of_match] = 1
                    }
                }
                else {
                    result[match.season] = {}
                    result[match.season][match.player_of_match] = 1
                }
                return result
            }, {})
            let output = Object.keys(player_of_matchs).reduce((result, season) => {
                let player = Object.entries(player_of_matchs[season]).sort((player1, player2) => {
                    return player2[1] - player1[1]
                })
                // console.log(player)
                result[season] = player[0][0]
                return result
            }, {})

            res.status(200)
                .json(output)
                .end()
        })
        .catch(err => {
            next({
                message: err,
                statusCode: 500
            })
        })
}

module.exports = highestPlayerOfMatchForSeasonMiddleware