const dotenv = require('dotenv')
const result = dotenv.config()


if (result.error) {
    console.error(result.error)
}
else {

    const { parsed: envs } = result
 
    module.exports = envs
}
